import 'package:flutter/material.dart';
import 'package:pogodynka/models/weather_response.dart';
import 'package:pogodynka/widgets/weather_icon_widget.dart';

class WeatherDetailsScreen extends StatelessWidget {

  final WeatherResponse _weather;

  WeatherDetailsScreen(this._weather);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          _getTopContainer(context),
          _detailsCard
        ],
      ),
    );
  }

  Widget _getTopContainer(context) => Stack(
    children: <Widget>[
      Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.longestSide * 0.4,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor
        ),
        child: WeatherIcon(_weather.name, _weather.weather.first.icon),
      ),
      Positioned(
        top: 8,
        left: 8,
        child: SafeArea(
          child: Material(
            color: Theme.of(context).primaryColor,
            child: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white,),
              onPressed: Navigator.of(context).pop,
            ),
          ),
        ),
      )
    ],
  );

  Widget get _detailsCard => Card(
    margin: const EdgeInsets.all(16.0),
    child: Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          _description,
          SizedBox(height: 16,),
          _sunriseSunset,
          SizedBox(height: 16,),
          _getField('Temperatura: ', '${_weather.main.temperature.toString()} \u00b0C', 0),
          SizedBox(height: 16,),
          _getField('Temperatura odczuwalna: ', '${_weather.main.feelsLike.toString()} \u00b0C', 0),
          SizedBox(height: 16,),
          _getField('Temperatura minimalna: ', '${_weather.main.tempMin.toString()} \u00b0C', 0),
          SizedBox(height: 16,),
          _getField('Temperatura maksymalna: ', '${_weather.main.tempMax.toString()} \u00b0C', 0),
          SizedBox(height: 16,),
          _getField('Ciśnienie: ', '${_weather.main.pressure.toInt().toString()} hPa', 0)
        ],
      ),
    ),
  );


  Widget get _description => Text(_weather.weather.first.description.replaceFirst(RegExp('[a-zA-Z]'), _weather.weather.first.description.substring(0, 1).toUpperCase()),
    textAlign: TextAlign.center,
  );

  Widget get _sunriseSunset => Row(
    children: <Widget>[
      _getField('Wschód słońca:', _weather.sun.formattedSunrise, 1),
      _getField('Zachód słońca:', _weather.sun.formattedSunset, 1),
    ],
  );

  Widget _getField(String fieldName, String value, int flex) => Expanded(
    flex: flex,
    child: Column(
      children: <Widget>[
        Text(fieldName, style: TextStyle(fontWeight: FontWeight.w500),),
        SizedBox(height: 4,),
        Text(value)
      ],
    ),
  );
}