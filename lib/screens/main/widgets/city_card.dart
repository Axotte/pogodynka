import 'package:flutter/material.dart';

typedef void OnClick(String name);

class CityCard extends StatelessWidget {
  final String cityName;
  final OnClick onClick;
  final Color color;

  CityCard({this.onClick, this.cityName, this.color: Colors.white});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        color: color,
        child: InkWell(
          onTap: () => onClick(cityName),
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(cityName),
          ),
        )
      ),
    );
  }

}