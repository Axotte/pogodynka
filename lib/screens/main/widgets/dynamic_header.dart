import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pogodynka/blocs/cities_bloc.dart';
import 'package:pogodynka/blocs/weather_bloc.dart';
import 'package:pogodynka/widgets/bordered_text_field.dart';
import 'package:pogodynka/widgets/raised_button.dart';

class DynamicHeader extends SliverPersistentHeaderDelegate {
  final FocusNode _nothing = FocusNode();
  final TextEditingController _searchController;
  final CitiesBloc _citiesBloc;
  final WeatherBloc _weatherBloc;
  final double maxHeight = 160.0;
  bool isTextFieldEnabled = true;

  DynamicHeader(this._citiesBloc, this._searchController, this._weatherBloc);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    _handleSizeChange(maxHeight - shrinkOffset, context);
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 32, 16, 0),
      child: Transform.scale(
        scale: (maxHeight - shrinkOffset) / maxHeight,
        child: OverflowBox(
          maxHeight: maxHeight,
          child: Column(
            children: <Widget>[
              _textField,
              _buttons
            ],
          ),
        ),
      ),
    );
  }

  @override
  double get maxExtent => maxHeight;

  @override
  double get minExtent => 0.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Widget get _textField => BorderedTextFiled(
    controller: _searchController,
    onChanged: _citiesBloc.getCities,
    enabled: isTextFieldEnabled,
    label: 'Miasto',
    suffixIcon: IconButton(
      icon: Icon(Icons.close),
      onPressed: _clearTextField,
    ),
  );

  void _clearTextField() {
    _citiesBloc.getCities();
    WidgetsBinding.instance.addPostFrameCallback((_) => _searchController.clear());
  }

  Widget get _buttons => ButtonBar(
    children: <Widget>[
      FlatButton(
        child: Row(
          children: <Widget>[
            Text("Moja Lokalizacja"),
            SizedBox(width: 4,),
            Icon(Icons.my_location)
          ],
        ),
        textColor: Colors.blue,
        color: Colors.transparent,
        onPressed: _onCurrentLocationClick,
      ),
      CustomRaisedButton(
        child: Text('Szukaj'),
        padding: EdgeInsets.symmetric(horizontal: 24),
        onPressed: _onSearchClick,
      ),
    ],
  );

  void _handleSizeChange(double height, context) {
    if(height < 100) {
      isTextFieldEnabled = false;
      FocusScope.of(context).requestFocus(_nothing);
    } else {
      isTextFieldEnabled = true;
    }
  }

  void _onSearchClick() {
    if(_searchController.text != null && _searchController.text.length > 1) {
      _weatherBloc.getWeatherForCity(_searchController.text);
    } else {
      _showErrorToast("Wpisz miasto");
    }
  }

  void _onCurrentLocationClick() {
    _weatherBloc.getWeatherForCords();
  }

  void _showErrorToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
    );
  }
}