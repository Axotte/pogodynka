
import 'package:flutter/material.dart';
import 'package:pogodynka/screens/main/main_content.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pogodynka'),
      ),
      body: Listener(
        onPointerDown: (_) => FocusScope.of(context).unfocus(),
        child: MainContent()
      ),
    );
  }
}