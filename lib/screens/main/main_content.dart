import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pogodynka/blocs/cities_bloc.dart';
import 'package:pogodynka/blocs/weather_bloc.dart';
import 'package:pogodynka/models/weather_response.dart';
import 'package:pogodynka/screens/details/weather_details_screen.dart';
import 'package:pogodynka/screens/main/widgets/city_card.dart';
import 'package:pogodynka/screens/main/widgets/dynamic_header.dart';

class MainContent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainContentState();
}

class MainContentState extends State<MainContent> {
  CitiesBloc _citiesBloc;
  WeatherBloc _weatherBloc;
  TextEditingController _searchController;
  StreamSubscription _weatherSubscription;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
    _citiesBloc = BlocProvider.getBloc();
    _weatherBloc = BlocProvider.getBloc();
    _weatherSubscription = _weatherBloc.weatherObservable.listen(_navigateToDetails);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _content,
        _loader,
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _weatherSubscription.cancel();
  }

  Widget get _content => StreamBuilder<List<String>>(
      stream: _citiesBloc.citiesObservable,
      builder: (context, snapshot) {
        return CustomScrollView(
          slivers: <Widget>[
            _header,
            _getCitiesList(snapshot)
          ],
        );
      }
  );

  Widget get _loader => StreamBuilder<bool>(
    stream: _weatherBloc.loadingWeatherObservable,
    initialData: false,
    builder: (context, snapshot) {
      if(snapshot.data) {
        return Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.grey.withAlpha(100),
          ),
          child: CircularProgressIndicator(),
        );
      } else {
        return Container();
      }
    },
  );

  SliverPersistentHeader get _header => SliverPersistentHeader(
    pinned: true,
    delegate: DynamicHeader(_citiesBloc, _searchController, _weatherBloc),
  );

  SliverList  _getCitiesList(AsyncSnapshot<List<String>> snapshot) => SliverList(
    delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
      return CityCard(
        onClick: _setCityName,
        color: index % 2 == 0 ? Color.fromRGBO(230, 240, 255, 1.0) : Colors.white,
        cityName: snapshot.data[index],
      );
    }, childCount: snapshot.data?.length ?? 0),
  );

  void _setCityName(String cityName) {
    _citiesBloc.getCities(cityName);
    _searchController.value = TextEditingValue(text: cityName);
  }

  void _navigateToDetails(WeatherResponse weather) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => WeatherDetailsScreen(weather)));
  }
}