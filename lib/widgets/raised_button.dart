import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef void OnClick();

class CustomRaisedButton extends StatelessWidget {
  final OnClick onPressed;
  final Widget child;
  final EdgeInsets padding;

  CustomRaisedButton({this.child, this.onPressed, this.padding});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: padding,
      onPressed: onPressed,
      child: child,
      color: Colors.blue,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      textColor: Colors.white,
    );
  }

}