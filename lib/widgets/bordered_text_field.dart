import 'package:flutter/material.dart';

typedef void ValueChanged(String value);

class BorderedTextFiled extends StatelessWidget{
  final TextEditingController controller;
  final ValueChanged onChanged;
  final ValueChanged onFieldSubmitted;
  final String label;
  final bool enabled;
  final Widget suffixIcon;

  BorderedTextFiled({this.controller, this.onFieldSubmitted, this.onChanged, this.label, this.enabled: true, this.suffixIcon});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      enabled: enabled,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        labelText: label,
        border: const OutlineInputBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(16.0)),
        )
      ),
    );
  }
}