import 'package:flutter/material.dart';

class WeatherIcon extends StatelessWidget {
  static const double _ICON_SIZE = 150;
  static const double _NAME_HEIGHT = 75;

  final String _weatherIcon;
  final String _cityName;

  WeatherIcon(this._cityName, this._weatherIcon);

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _getIcon(context),
          _getName(context)
        ],
      ),
    );
  }

  Widget  _getName(context) => Positioned(
    child: Padding(
      padding: const EdgeInsets.only(top: _ICON_SIZE * 1.25),
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.shortestSide * 0.7,
        height: _NAME_HEIGHT,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(const Radius.circular(8.0)),
          color: Colors.white,
          boxShadow: _shadow
        ),
        child: Text(_cityName,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 21,
            fontWeight: FontWeight.w500
          ),
        ),
      ),
    ),
  );

  Widget  _getIcon(context) => Container(
    width: _ICON_SIZE,
    padding: const EdgeInsets.only(bottom: 8),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.white),
      color: Theme.of(context).primaryColor,
      shape: BoxShape.circle,
      boxShadow: _shadow
    ),
    child: Image.asset('assets/icons/$_weatherIcon.png'),
  );

  List<BoxShadow> get _shadow => [
    BoxShadow(color: Colors.black12, spreadRadius: 2.0, offset: Offset(0.0, 5.0), blurRadius: 2.0)
  ];
}