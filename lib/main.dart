import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:pogodynka/blocs/cities_bloc.dart';
import 'package:pogodynka/blocs/weather_bloc.dart';
import 'package:pogodynka/repositories/cities_repository.dart';
import 'package:pogodynka/repositories/weather_repository.dart';
import 'package:pogodynka/screens/main/main_screen.dart';

void main() => runApp(WeatherApp());

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: _blocs,
      dependencies: _dependencies,
      child: MaterialApp(
        title: 'Pogodynka',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainScreen(),
      ),
    );
  }

  List<Bloc> get _blocs => [
    Bloc((i) => CitiesBloc(i.get())),
    Bloc((i) => WeatherBloc(i.get())),
  ];

  List<Dependency> get _dependencies => [
    Dependency((_) => WeatherRepository()),
    Dependency((_) => CitiesRepository()),
  ];
}

