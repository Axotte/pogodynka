import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:pogodynka/repositories/cities_repository.dart';
import 'package:rxdart/rxdart.dart';

class CitiesBloc extends BlocBase {
  CitiesRepository _citiesRepository;

  CitiesBloc(this._citiesRepository) {
    getCities();
  }

  BehaviorSubject<List<String>> _citiesSubject = BehaviorSubject();

  Stream<List<String>> get citiesObservable => _citiesSubject.stream;

  getCities([String pattern]) async {
    _citiesSubject.add(await _citiesRepository.getCities(pattern));
  }

  @override
  void dispose() {
    super.dispose();
    _citiesSubject.close();
  }
}