import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:pogodynka/models/weather_response.dart';
import 'package:pogodynka/repositories/weather_repository.dart';
import 'package:pogodynka/utils/location_util.dart';
import 'package:rxdart/rxdart.dart';

class WeatherBloc extends BlocBase {
  final WeatherRepository _weatherRepository;

  WeatherBloc(this._weatherRepository);

  BehaviorSubject<WeatherResponse> _weatherSubject = BehaviorSubject();
  Stream<WeatherResponse> get weatherObservable => _weatherSubject.stream;

  BehaviorSubject<bool> _loadingWeatherSubject = BehaviorSubject();
  Stream<bool> get loadingWeatherObservable => _loadingWeatherSubject.stream;

  Future getWeatherForCity(String city) async {
    _loadingWeatherSubject.add(true);
    _weatherRepository.getWeatherForCity(city)
        .then(_onWeatherSuccess)
        .catchError(_onWeatherError);
  }

  Future getWeatherForCords() async {
    _loadingWeatherSubject.add(true);
    LocationUtil.getLocation().then((location) {
      _weatherRepository.getWeatherForCoords(location.lat, location.lon)
          .then(_onWeatherSuccess)
          .catchError(_onWeatherError);
    }).catchError(_onWeatherError);
  }

  Future _onWeatherSuccess(WeatherResponse weather) async {
    _loadingWeatherSubject.add(false);
    _weatherSubject.add(weather);
  }

  Future _onWeatherError(e) async {
    _loadingWeatherSubject.add(false);
    _weatherSubject.addError(e);
  }

  @override
  void dispose() {
    super.dispose();
    _weatherSubject.close();
    _loadingWeatherSubject.close();
  }
}