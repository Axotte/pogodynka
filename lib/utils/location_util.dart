import 'package:flutter/services.dart';
import 'package:pogodynka/models/location_model.dart';

class LocationUtil {
  static MethodChannel _channel = MethodChannel('com.example.pogodynka/method-channel');
  
  static Future<LocationModel> getLocation() {
    return _channel.invokeMethod('getLocation').then((result) {
      var lat = (result as Map)['lat'];
      var lon = (result as Map)['lon'];
      return LocationModel(lat, lon);
    }).catchError((e) {
      print(e);
      return null;
    });
  }
}