import 'package:pogodynka/dataSources/remote/api_client.dart';
import 'package:pogodynka/models/weather_response.dart';

const String units = 'metric';
const String lang = 'pl';
const String apiKey = 'a874c0c655975af157a5e75f86bb9472';

class WeatherRepository {
  Future<WeatherResponse> getWeatherForCity(String city) => client.getWeatherForCity(city, lang, units, apiKey);

  Future<WeatherResponse> getWeatherForCoords(double lat, double lon) => client.getWeatherForCoords(lat, lon, lang, units, apiKey);
}