
import 'package:pogodynka/dataSources/local/cities.dart';

class CitiesRepository {
  Future<List<String>> getCities([String patten]) async =>
      patten == null ? cities : cities.where((city) => city.toLowerCase().contains(patten.toLowerCase())).toList();
}