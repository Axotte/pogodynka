package com.example.pogodynka

import android.content.pm.PackageManager
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import io.nlopez.smartlocation.SmartLocation

class MainActivity: FlutterActivity() {
    companion object {
        const val CHANNEL = "com.example.pogodynka/method-channel"
        const val REQUEST_CODE = 692137420
    }

    var result: MethodChannel.Result? = null

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            when(call.method) {
                "getLocation" -> {
                    getLocation(result)
                }
            }
        }
    }

    private fun getLocation(result: MethodChannel.Result) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.result = result
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)
        } else {
            SmartLocation.with(this).location().oneFix().start {
                result.success(hashMapOf("lat" to it.latitude, "lon" to it.longitude))
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(REQUEST_CODE == requestCode) {
            if(grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                SmartLocation.with(this).location().oneFix().start {
                    result?.success(hashMapOf("lat" to it.latitude, "lon" to it.longitude))
                    result = null
                }
            } else {
                result?.error("Permission Error", "No location permission", null)
                result = null
            }
        } else {
            result = null
        }
    }
}
